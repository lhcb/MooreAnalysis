# :warning: MooreAnalysis has been retired :warning:

The MooreAnalysis project was defined as a project dependent on Moore and Analysis, required for the 
HltEfficiencyChecker package. With Analysis being retired and its contents being moved to DaVinci, 
HltEfficiencyChecker was also moved to [DaVinci](https://gitlab.cern.ch/lhcb/DaVinci).
All other code in MooreAnalysis had been removed by this point, and so MooreAnalysis was retired.

This was mainly discussed in [MooreAnalysis#45](https://gitlab.cern.ch/lhcb/MooreAnalysis/-/issues/45) and 
[Analysis#52](https://gitlab.cern.ch/lhcb/Analysis/-/issues/52).

## Move ongoing (unmerged) work to DaVinci

You can rebase your unmerged branches on top of DaVinci and open MRs there.

First, add MooreAnalysis as a remote in your DaVinci clone:

```sh
cd DaVinci
git remote add MooreAnalysis ssh://git@gitlab.cern.ch:7999/lhcb/MooreAnalysis.git
git fetch MooreAnalysis
git fetch origin
```

And then rebase the MooreAnalysis commits in your branch (e.g. `Topo_Neural_Net`)
onto the `master` of DaVinci:

```sh
# interactive rebase: you can just save the default list
git rebase -i --onto origin/master MooreAnalysis/master MooreAnalysis/Topo_Neural_Net
# create a new branch on the rebased commits and push
git checkout -b Topo_Neural_Net
git push -u origin Topo_Neural_Net
```
